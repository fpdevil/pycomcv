# My Experiments with Computer Vision Using Python

Some random exrcises for image and vision handling using `Python`.

> Following are some of the major python bindings and libraries used for this

- `opencv 3.x.x`
- `mahootas`
- `numpy`
- `matplotlib`
- `scipy`
- `scikitlearn`
